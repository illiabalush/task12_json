package com.balush.view;

import com.balush.controller.Controller;
import com.balush.interfaces.Printable;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ConsoleView extends View {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> menuItem;
    private Scanner scanner;

    public ConsoleView() {
        controller = new Controller(this);
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - read json");
        menu.put("2", "2 - write to file [use after reading]");
        menu.put("Q", "Q - exit");
        menuItem = new LinkedHashMap<>();
        menuItem.put("1", this::startParser);
        menuItem.put("2", this::writeJson);
        scanner = new Scanner(System.in);
    }

    private void startParser() {
        controller.getModel().printJsonList();
    }

    private void writeJson() {
        controller.getModel().writeToJsonFile();
    }

    @Override
    public void start() {
        String keyMenu;
        do {
            showMenu();
            scanner = new Scanner(System.in);
            logger.info("Please, select menu point:");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                menuItem.get(keyMenu).print();
            } catch (Exception ignored) {

            }
        } while (!keyMenu.equals("Q"));
    }

    @Override
    public void showMenu() {
        logger.info("\n");
        for (String i : menu.values()) {
            logger.info(i);
        }
    }
}
