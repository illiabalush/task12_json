package com.balush.model;

import com.balush.model.comparator.KnifeComparator;
import com.balush.model.jsonschema.JSONParser;
import com.balush.model.jsonschema.JSONSchemeValidator;
import com.balush.model.objects.Knife;
import com.balush.view.View;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class Model {
    private View view;
    private List<Knife> knives;

    public Model(View view) {
        this.view = view;
        startJson();
    }

    private void startJson() {
        final File jsonPath = new File("src/main/resources/knifeJSON.json");
        final File schemaPath = new File("src/main/resources/knifeJSONScheme.json");

        JSONParser parser = new JSONParser();
        try {
            if (JSONSchemeValidator.validateJSON(jsonPath, schemaPath)) {
                knives = parser.getKnifeList(jsonPath);
                knives.sort(new KnifeComparator());
            } else {
                view.showMessage("Not valid!");
            }
        } catch (IOException | ProcessingException e) {
            e.printStackTrace();
        }
    }

    public void writeToJsonFile() {
        JSONArray knifeList = new JSONArray();
        for (Knife knife : knives) {
            JSONObject jsonObject = new JSONObject();
            JSONObject visualJson = new JSONObject();
            JSONObject handlesJson = new JSONObject();
            jsonObject.put("type", knife.getType());
            jsonObject.put("handly", knife.getHandly());
            jsonObject.put("origin", knife.getOrigin());
            visualJson.put("length", knife.getVisual().getLength());
            visualJson.put("width", knife.getVisual().getWidth());
            visualJson.put("material", knife.getVisual().getMaterial());
            handlesJson.put("handlesType", knife.getVisual().getHandles().getHandlesType());
            handlesJson.put("handleSubtype", knife.getVisual().getHandles().getHandleSubtype());
            visualJson.put("handles", handlesJson);
            visualJson.put("bloodFlows", knife.getVisual().isBloodFlows());
            jsonObject.put("visual", visualJson);
            jsonObject.put("value", knife.getValue());
            knifeList.add(jsonObject);
        }
        try {
            FileWriter fileWriter = new FileWriter("src/main/resources/customKnifeJSON.json");
            fileWriter.write(knifeList.toJSONString());
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printJsonList() {
        view.showMessage("JSON");
        for (Knife knife : knives) {
            view.showMessage(knife.toString());
        }
    }
}
