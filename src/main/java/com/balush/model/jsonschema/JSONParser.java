package com.balush.model.jsonschema;

import com.balush.model.objects.Knife;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JSONParser {
    private ObjectMapper objectMapper;

    public JSONParser() {
        this.objectMapper = new ObjectMapper();
    }

    public List<Knife> getKnifeList(File jsonFile) {
        List<Knife> knives = new ArrayList<>();
        try {
            knives = Arrays.asList(objectMapper.readValue(jsonFile, Knife[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return knives;
    }
}
