package com.balush.model.objects;

public class Visual {
    private int length;
    private int width;
    private String material;
    private Handles handles;
    private boolean isBloodFlows;

    public Visual() {
    }

    public Visual(int length, int width, String material, Handles handles, boolean isBloodFlows) {
        this.length = length;
        this.width = width;
        this.material = material;
        this.handles = handles;
        this.isBloodFlows = isBloodFlows;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public Handles getHandles() {
        return handles;
    }

    public void setHandles(Handles handles) {
        this.handles = handles;
    }

    public boolean isBloodFlows() {
        return isBloodFlows;
    }

    public void setBloodFlows(boolean bloodFlows) {
        this.isBloodFlows = bloodFlows;
    }

    @Override
    public String toString() {
        return  "Length: " + length + "\n" +
                "Width: " + width + "\n" +
                "Material: " + material + "\n" +
                 handles +
                "Blood flows: " + isBloodFlows + "\n";
    }
}
