package com.balush.model.objects;

public class Knife {
    private String type;
    private String handly;
    private String origin;
    private Visual visual;
    private String value;

    public Knife() {
    }

    public Knife(String type, String handly, String origin, Visual visual, String value) {
        this.type = type;
        this.handly = handly;
        this.origin = origin;
        this.visual = visual;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHandly() {
        return handly;
    }

    public void setHandly(String handly) {
        this.handly = handly;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Visual getVisual() {
        return visual;
    }

    public void setVisual(Visual visual) {
        this.visual = visual;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "\nType: " + type + "\n" +
                "Handly: " + handly + "\n" +
                "Origin: " + origin + "\n" +
                visual +
                "Value: " + value + "\n";
    }
}
