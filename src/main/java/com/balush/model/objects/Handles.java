package com.balush.model.objects;

public class Handles {
    private String handlesType;
    private String handleSubtype;

    public Handles() {
    }

    public Handles(String handlesType, String handleSubtype) {
        this.handlesType = handlesType;
        this.handleSubtype = handleSubtype;
    }

    public String getHandlesType() {
        return handlesType;
    }

    public void setHandlesType(String handlesType) {
        this.handlesType = handlesType;
    }

    public String getHandleSubtype() {
        return handleSubtype;
    }

    public void setHandleSubtype(String handleSubtype) {
        this.handleSubtype = handleSubtype;
    }

    @Override
    public String toString() {
        return  "Handle Type: " + handlesType + "\n" +
                "Handle subtype: " + handleSubtype + "\n";
    }
}
